#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

SYSTEMDUSERDIR="${XDG_CONFIG_HOME:-"${HOME}/.config"}/systemd/user"

SERVICEANDTIMER_NAME="thbd-bkp"
SERVICEANDTIMER="${DIR}/${SERVICEANDTIMER_NAME}"


mkdir -p "${SYSTEMDUSERDIR}"
systemctl --user disable --now "${SERVICEANDTIMER_NAME}.timer"     &>/dev/null
systemctl --user stop          "${SERVICEANDTIMER_NAME}.service"   &>/dev/null

rm -f "${SYSTEMDUSERDIR}/${SERVICEANDTIMER_NAME}.service"
systemctl --user daemon-reload

systemctl --user link "${SERVICEANDTIMER}.service"
systemctl --user enable --now "${SERVICEANDTIMER}.timer"

