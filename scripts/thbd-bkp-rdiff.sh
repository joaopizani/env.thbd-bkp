#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

TBPROFILENAME_DEFAULT="*.default"
TBPROFILEDIR_DEFAULT="${HOME}/.thunderbird/${TBPROFILENAME_DEFAULT}"
TBPROFILEDIR="$(readlink -m "${1:-"${TBPROFILEDIR_DEFAULT}"}")"

FINALTRGDIR_DEFAULT="$(xdg-user-dir DOWNLOAD)/thunderbird-personal-bkp"
FINALTRGDIR="$(readlink -m "${2:-"${FINALTRGDIR_DEFAULT}"}")"

MAXNUMDIFFS_DEFAULT=50
MAXNUMDIFFS="${3:-"${MAXNUMDIFFS_DEFAULT}"}"


TMPTRGDIR="$(mktemp --quiet --tmpdir --directory "thbd-and-rdiff_XXXXX")"
"${DIR}/thbd-bkp-simple.sh" "${TBPROFILEDIR}" "${TMPTRGDIR}"


shopt -q -s nullglob;  TMPTRGDIRFILES=( "${TMPTRGDIR}"/* );  shopt -q -u nullglob
shopt -q -s nullglob;  FINALTRGDIRFILES=( "${FINALTRGDIR}"/* );  shopt -q -u nullglob
RDIFF_FROM="${TMPTRGDIRFILES[0]}"  # should be the only file in there
FINALTRGDIR_TO="${FINALTRGDIRFILES[-1]}"  # take latest, become the tmp's second-latest
RDIFF_TO="${TMPTRGDIR}/$(basename "${FINALTRGDIR_TO}")"  # FROM=latest, TO=second-latest

cp "${FINALTRGDIR_TO}" "${RDIFF_TO}"
"${DIR}/rdiff-patch.sh" remove "${RDIFF_FROM}" "${RDIFF_TO}" "/tmp"
rm -f "${FINALTRGDIR_TO}"  # remove the one which WAS the latest until now
mv "${TMPTRGDIR}"/* "${FINALTRGDIR}"/  # move the patch and the new latest to the finaltrgdir
rmdir "${TMPTRGDIR}"


shopt -q -s nullglob;  FINALTRGDIRFILES=( "${FINALTRGDIR}"/* );  shopt -q -u nullglob
NUMFINALTRGDIRFILES="${#FINALTRGDIRFILES[@]}"
CURNUMDIFFS=$(( NUMFINALTRGDIRFILES - 1 ))
if [ $CURNUMDIFFS -gt $MAXNUMDIFFS ]; then
  DELNUMDIFFS=$(( CURNUMDIFFS - MAXNUMDIFFS ))
  for (( i = 0 ; i < ${DELNUMDIFFS} ; i++ )); do
    rm -f "${FINALTRGDIRFILES[${i}]}"
  done
fi
