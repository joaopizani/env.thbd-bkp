#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

REMOVE_TO_DEFAULT="keep"
REMOVE_TO="${1:-"${REMOVE_TO_DEFAULT}"}"

# By default the last in the directory is FROM and the second-to-last is TO.
# This makes for a nice delta logic going back in time if files are chronologically named.
FILES_PWD=( "$(pwd)"/* )
FILE_FROM_DEFAULT="${FILES_PWD[-1]}"
FILE_TO_DEFAULT="${FILES_PWD[-2]}"

FILE_FROM="$(readlink -f "${2:-"${FILE_FROM_DEFAULT}"}")"
FILE_TO="$(readlink -f "${3:-"${FILE_TO_DEFAULT}"}")"
FILE_FROM_NAME="$(basename "${FILE_FROM}")"
FILE_TO_NAME="$(basename "${FILE_TO}")"

XDGDOWNLOADS="$(xdg-user-dir DOWNLOAD)"
TMPROOTDIR_DEFAULT="${XDGDOWNLOADS:-"${HOME}/Downloads"}"
TMPROOTDIR="$(readlink -f "${4:-"${TMPROOTDIR_DEFAULT}"}")"
TMPCOPYDIR="$(mktemp --quiet --tmpdir="${TMPROOTDIR}" --directory "rdiff-patch_XXXXX")"

FILE_PATCH_DEFAULT="$(dirname "${FILE_FROM}")/00rdiff__${FILE_FROM_NAME}__to__${FILE_TO_NAME}"
FILE_PATCH="$(readlink -f "${5:-"${FILE_PATCH_DEFAULT}"}")"
mkdir -p "$(dirname "${FILE_PATCH}")"


TMPCOPYFROM="${TMPCOPYDIR}/${FILE_FROM_NAME}"
cp "${FILE_FROM}" "${TMPCOPYFROM}"
rdiff signature "${TMPCOPYFROM}" "${TMPCOPYDIR}/sig"
rm -f "${TMPCOPYFROM}"

TMPCOPYTO="${TMPCOPYDIR}/${FILE_TO_NAME}"
cp "${FILE_TO}" "${TMPCOPYTO}"
rdiff delta "${TMPCOPYDIR}/sig" "${TMPCOPYTO}" "${TMPCOPYDIR}/delta"
rm -f "${TMPCOPYDIR}/sig"  &&  rm -f "${TMPCOPYTO}"
cp "${TMPCOPYDIR}/delta" "${FILE_PATCH}"
rm -f "${TMPCOPYDIR}/delta"
rmdir "${TMPCOPYDIR}"

[[ "${REMOVE_TO}" = "remove" ]]  &&  rm -f "${FILE_TO}"
