#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"


TBPROFILEDIR_DEFAULT="${HOME}/.thunderbird"/*.default
FINALTRGDIR_DEFAULT="$(xdg-user-dir DOWNLOAD)/thunderbird-personal-bkp"

TBPROFILEDIR="$(readlink -m ${1:-${TBPROFILEDIR_DEFAULT}})"
TBPROFILENAME="$(basename "${TBPROFILEDIR}")"
FINALTRGDIR="$(readlink -m "${2:-"${FINALTRGDIR_DEFAULT}"}")"

MAILSRCNAME="ImapMail"
MAILSRCDIR="${TBPROFILEDIR}/${MAILSRCNAME}"

TMPDIR="/tmp"
PROFBKPNAME="thbd-bkp-$(date '+%Y%m%dT%H%M')-${TBPROFILENAME}"
PROFBKPDIR="${TMPDIR}/${PROFBKPNAME}"
mkdir -p "${PROFBKPDIR}"

MAILBKPDIR="${PROFBKPDIR}/${MAILSRCNAME}"


mv "${MAILSRCDIR}" "${TBPROFILEDIR}/../${MAILSRCNAME}"
cp -r "${TBPROFILEDIR}/"* "${PROFBKPDIR}/"
mv "${TBPROFILEDIR}/../${MAILSRCNAME}" "${MAILSRCDIR}"
"${DIR}/00-deps/delatt-maildir" "${MAILSRCDIR}" "${MAILBKPDIR}"

tar -cf - -C "${TMPDIR}" "${PROFBKPNAME}" | gzip --rsyncable - > "${PROFBKPDIR}.tar.gz"
mkdir -p "${FINALTRGDIR}"
mv "${PROFBKPDIR}.tar.gz" "${FINALTRGDIR}/"
rm -rf "${PROFBKPDIR}"
