#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

SYSD_USER_BINARIES="$(systemd-path user-binaries)"
BINDIR="${SYSD_USER_BINARIES:-"${HOME}/.local/bin"}"


RELPATHS_TO_LINK=(
    'thbd-bkp-simple.sh'
    'thbd-bkp-rdiff.sh'
)

SCRIPTDIR="${DIR}/scripts"

mkdir -p "${BINDIR}"
for scr in "${RELPATHS_TO_LINK[@]}"; do ln -sfnr "${SCRIPTDIR}/${scr}" "${BINDIR}/"; done


sudo apt install libemail-mime-perl rdiff


"${DIR}/systemd-user-units/install.sh"

